# Mirror Text

Mirror Text - An Inkscape Extension

 Inkscape 1.1 +

 https://inkscape.org/~inklinea/resources/=extension/

▶ This is a hideously inefficient extension.
  Uses the command line to loop through each
  text element and flip it horizontally.

  ▶ If you have made sentences or labels by aligning
  separate individual text elements they will be flipped
  separately so 45 mph would become mph 45

![](https://gitlab.com/inklinea/image_store/-/raw/main/Baubel_2021-11-30-23-49-46.jpg)
  


