#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Flip Text - flip each text element separately in a document
#  After setting the options in the main dialogue
#  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to org.inkscape.inklinea.flip_text.noprefs
#  For shortcut triggered quick export
#  Requires Inkscape 1.1+ -->
# #############################################################################
import random

import inkex
from inkex import command
from pathlib import Path
from datetime import datetime
import tempfile, shutil, os
from lxml import etree
import random

import time

conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}

def get_attributes(self):
    for att in dir(self):
        inkex.errormsg((att, getattr(self, att)))

def make_temp_svg(self, temp_filename, temp_dir):

    temp_filename_path = os.path.join(temp_dir.name, temp_filename)
    temp_file = open(temp_filename_path, 'w')
    my_svg_string = self.svg.root.tostring().decode("utf-8")
    temp_file.write(my_svg_string)
    temp_file.close()

    return temp_filename_path


def command_line_call(self):
    # make a temp directory
    temp_dir = tempfile.TemporaryDirectory()

    temp_filename = 'temp_svg_' + str(random.randrange(10000, 99999)) + '.svg'
    temp_svg_filename = make_temp_svg(self, temp_filename, temp_dir)
    my_temp_svg_filename_path = temp_svg_filename

    flip_type = self.options.flip_type_radio

    if flip_type == 'image_and_text':
        selection_type = f'select-all:all;ObjectFlipHorizontally;select-clear;'
        text_elements = self.svg.xpath('//svg:text')
    elif flip_type == 'text_only':
        selection_type = f''
        text_elements = self.svg.xpath('//svg:text')
    elif flip_type == 'selected_text_only':
        if len(self.svg.selected) < 1:
            temp_dir.cleanup()
            return
        else:
            text_elements = []
            for my_object in self.svg.selected:
                if my_object.TAG == 'text':
                    text_elements.append(my_object)
                if len(text_elements) < 1:
                    temp_dir.cleanup()
                    return
            selection_type = f''


    text_elements_id_list = []

    for text_element in text_elements:
        text_element_id = text_element.get_id()

        text_elements_id_list.append('select-by-id:' + str(text_element_id) + ';ObjectFlipHorizontally;select-clear')

    text_element_ids_cli = ";".join(text_elements_id_list)

    # Build a formatted string for command line actions

    # --batch-process ( or --with-gui ) is required if verbs are used in addition to actions
    my_options = '--batch-process'

    my_actions = '--actions='

    my_actions = my_actions + f'\
    {selection_type} \
    {text_element_ids_cli}; \
    FileSave;'

    my_actions = my_actions.replace(' ', '')

    inkex.command.inkscape(my_temp_svg_filename_path, my_options, my_actions)

    temp_svg_file = open(temp_svg_filename, 'r')
    temp_svg_file.seek(0)

    self.document = inkex.load_svg(temp_svg_file.read().encode())

    # Close temp file and directory
    temp_svg_file.close()
    temp_dir.cleanup()

class FlipText(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--mirror_text_notebook", type=str, dest="mirror_text_notebook_page", default=0)

        pars.add_argument("--flip_type_radio", type=str, dest="flip_type_radio", default='none')

    def effect(self):

        command_line_call(self)


if __name__ == '__main__':
    FlipText().run()
