<?xml version="1.0" encoding="UTF-8"?>

<!--Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  Mirror Text   -->
<!--        #  After setting the options in the main dialogue-->
<!--        #  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to org.inkscape.inklinea.flip_text.noprefs-->
<!--        #  For shortcut triggered quick export-->
<!--        # Requires Inkscape 1.1+ -->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Mirror Text</name>
    <id>org.inkscape.inklinea.mirror_text</id>

    <param name="mirror_text_notebook" type="notebook">

        <page name="settings_page" gui-text="Settings">
            <hbox>
            <param name="flip_type_radio" type="optiongroup" appearance="radio" gui-text="Flip">
                <option value="image_and_text">Image and Text</option>
                <option value="text_only">Text Only</option>
                <option value="selected_text_only">Selected Text Only</option>
            </param>
            </hbox>
            <hbox>

                <label xml:space="preserve">
    ▶ Can be used from the command line
   inkscape --batch-process --actions="org.inkscape.inklinea.flip_text;export-filename:flipped.svg;export-do;" ./drawing.svg
		</label>

            </hbox>
        </page>

        <page name="about_page" gui-text="About">

            <label>
                Mirror Text - An Inkscape Extension
            </label>
            <label>
                Inkscape 1.1 +
            </label>
            <label appearance="url">

                https://inkscape.org/~inklinea/resources/=extension/

            </label>

            <label xml:space="preserve">
▶ This is a hideously inefficient extension.
  Uses the command line to loop through each
  text element and flip it horizontally.

  ** Please Note ***

▶ If you have made sentences or labels by aligning
  separate individual text elements they will be flipped
  separately so 45 mph would become mph 45
		</label>
        </page>

    </param>


    <effect>
        <object-type>path</object-type>
        <effects-menu>
            <submenu name="Text"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">mirror_text.py</command>
    </script>
</inkscape-extension>
